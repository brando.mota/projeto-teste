FROM nginx

MAINTAINER Rodrigo Rezende <rodrigo@rznd.com.br>

COPY ./dist /usr/share/nginx/html
